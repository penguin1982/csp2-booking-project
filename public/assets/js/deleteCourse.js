let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');
let deleteCourseUrl = '';
const msgContainer = document.getElementById('msg-container');

let token = localStorage.getItem('token');
isAdmin = localStorage.getItem('isAdmin'); //initialized at script.js

if (location.hostname === 'localhost' || location.hostname === '127.0.0.1' || location.hostname === '') {
	deleteCourseUrl = `http://localhost:4000/api/courses/${courseId}`;
}
else {
	deleteCourseUrl = `https://quiet-springs-95074.herokuapp.com/api/courses/${courseId}`;
}

if (!token || token === null || isAdmin === 'false') {
	window.location.href = './courses.html';
}
else {
	const config = {
		method: 'DELETE',
		headers: {
			'Content-Type': 'application/json',
			Authorization: `Bearer ${token}`
		}
	};

	fetch(deleteCourseUrl, config).then((res) => res.json()).then((data) => {
		if (data === true) {
			msgContainer.innerHTML = 'Your request has been saved';
		}
		else {
			msgContainer.innerHTML = 'Something went wrong';
		}
	});
}
