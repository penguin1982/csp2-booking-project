const navItems = document.getElementById('navSession');

let userToken = localStorage.getItem('token');
let isAdmin = localStorage.getItem('isAdmin');

// const loc = window.location.pathname;
// console.log(loc);
// const dir = loc.substring(0, loc.lastIndexOf('/'));
// console.log(dir);

// console.log(location.hostname);
// console.log(typeof location.hostname);

if (location.hostname === 'localhost' || location.hostname === '127.0.0.1' || location.hostname === '') {
	if (!userToken) {
		navItems.innerHTML = `<li class="nav-item">
        <a href="/csp2-booking-project/frontend/public/pages/login.html" class="nav-link">Log in</a>
    </li>
    <li class="nav-item">
        <a href="/csp2-booking-project/frontend/public/pages/register.html" class="nav-link">Register</a>
    </li>
    `;
	}
	else if (isAdmin == 'true') {
		navItems.innerHTML = `<li class="nav-item">
        <a href="/csp2-booking-project/frontend/public/pages/logout.html" class="nav-link">Log Out</a>
    </li>
    <li class="nav-item">
        <a href="/csp2-booking-project/frontend/public/pages/addcourse.html" class="nav-link">Add Course</a>
    </li>
    `;
	}
	else {
		navItems.innerHTML = `<li class="nav-item">
        <a href="/csp2-booking-project/frontend/public/pages/logout.html" class="nav-link">Log Out</a>
    </li>
    <li class="nav-item">
        <a href="/csp2-booking-project/frontend/public/pages/profile.html" class="nav-link">Profile</a>
    </li>
    `;
	}
}
else {
	if (!userToken) {
		navItems.innerHTML = `<li class="nav-item">
            <a href="/csp2-booking-project/pages/login.html" class="nav-link">Log in</a>
        </li>
        <li class="nav-item">
            <a href="/csp2-booking-project/pages/register.html" class="nav-link">Register</a>
        </li>
        `;
	}
	else if (isAdmin == 'true') {
		navItems.innerHTML = `<li class="nav-item">
            <a href="/csp2-booking-project/pages/logout.html" class="nav-link">Log Out</a>
        </li>
        <li class="nav-item">
            <a href="/csp2-booking-project/pages/addcourse.html" class="nav-link">Add Course</a>
        </li>
        `;
	}
	else {
		navItems.innerHTML = `<li class="nav-item">
            <a href="/csp2-booking-project/pages/logout.html" class="nav-link">Log Out</a>
        </li>
        <li class="nav-item">
            <a href="/csp2-booking-project/pages/profile.html" class="nav-link">Profile</a>
        </li>
        `;
	}
}
