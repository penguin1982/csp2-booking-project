const courseContainer = document.getElementById('coursesContainer');
const modalButton = document.getElementById('adminButton');
let cardFooter;

let token = localStorage.getItem('token');
isAdmin = localStorage.getItem('isAdmin'); //inialized in script.js
let userId = localStorage.getItem('id');

// console.log(token, typeof token);
// console.log(isAdmin, typeof isAdmin);

let getCoursesUrl = '';
const config = {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json',
		Authorization: `Bearer ${token}`
	}
};

if (location.hostname === 'localhost' || location.hostname === '127.0.0.1' || location.hostname === '') {
	getCoursesUrl = 'http://localhost:4000/api/courses';
}
else {
	getCoursesUrl = 'https://quiet-springs-95074.herokuapp.com/api/courses';
}

fetch(getCoursesUrl, config).then((res) => res.json()).then((data) => {
	let courseData;
	if (data.length < 1) {
		courseData = 'No course Available';
	}
	else {
		courseData = data
			.map((course) => {
				const isEnroll = course.enrollees.find((enrollee) => enrollee.userId == userId);
				console.log(isEnroll);
				if (isAdmin === null && token === null) {
					// <a href="./login.html" class="btn btn-secondary text-white btn-block">Enroll</a>
					cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">View course details</a>					
					`;
				}
				else if (isAdmin === 'false' && token) {
					cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">View course details</a>
					`;
				}
				else if (isAdmin === 'true' && token) {
					cardFooter = `
					<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">View course details</a>
					<a href="./editCourse.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Edit</a>
					<a href="./deleteCourse.html?courseId=${course._id}" class="btn ${course.isActive
						? 'btn-info'
						: 'btn-secondary'} text-white btn-block">${course.isActive ? 'Disable' : 'Enable'}</a>
					`;
				}
				else {
					cardFooter = '';
				}

				return `
					<div class="col-md-6 my-3">
						<div class="card">
							<div class="card-body">
								<div>
									<h5 class="card-title ${course.isActive ? 'active' : 'inactive'} ${isEnroll
					? 'enrolled'
					: ''}">${course.name}</h5>
								</div>
								<p class="card-text text-left">${course.description}</p>
								<p class="card-text text-left">₱${course.price}</p>
								
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
						</div>
					</div>
					`;
			})
			.join('');
	}
	courseContainer.innerHTML = courseData;
});
