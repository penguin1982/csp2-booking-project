const createCourse = document.querySelector('#createCourse');

let token = localStorage.getItem('token');
isAdmin = localStorage.getItem('isAdmin'); //initialized at script.js

if (!token || token === null || isAdmin === 'false') {
	window.location.href = './courses.html';
}

createCourse.addEventListener('submit', (e) => {
	e.preventDefault();
	const courseName = document.querySelector('#courseName').value;
	const coursePrice = document.querySelector('#coursePrice').value;
	const courseDescription = document.querySelector('#courseDescription').value;

	const courseExistURL = '';
	const addCourseURL = '';

	if (location.hostname === 'localhost' || location.hostname === '127.0.0.1' || location.hostname === '') {
		courseExistURL = 'http://localhost:4000/api/courses/course-exists';
		addCourseURL = 'http://localhost:4000/api/courses/addCourse';
	}
	else {
		courseExistURL = 'https://quiet-springs-95074.herokuapp.com/api/courses/course-exists';
		addCourseURL = 'https://quiet-springs-95074.herokuapp.com/api/courses/addCourse';
	}

	const config = {
		method: 'POST',
		headers: { 'Content-Type': 'application/json' },
		body: JSON.stringify({
			name: courseName,
			description: courseDescription,
			price: coursePrice
		})
	};

	fetch(courseExistURL, config).then((res) => res.json()).then((data) => {
		if (!data) {
			fetch(addCourseURL, config)
				.then((res) => {
					return res.json();
				})
				.then((data) => {
					if (data) {
						alertify.set('notifier', 'position', 'top-center');
						alertify.success('New course added', 6);
						createCourse.reset();
					}
					else {
						alertify.set('notifier', 'position', 'top-center');
						alertify.error('Something went wrong');
					}
				});
		}
		else {
			alertify.set('notifier', 'position', 'top-center');
			alertify.error('Course already exist');
		}
	});
});
