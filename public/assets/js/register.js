const formRegister = document.querySelector('#registerUser');

formRegister.addEventListener('submit', (e) => {
	e.preventDefault();

	const firstName = document.querySelector('#firstName').value;
	const lastName = document.querySelector('#lastName').value;
	const userEmail = document.querySelector('#userEmail').value;
	const mobileNumber = document.querySelector('#mobileNumber').value;
	const password1 = document.querySelector('#password1').value;
	const password2 = document.querySelector('#password2').value;
	let urlCheckEmail = '';
	let urlRegister = '';

	if (location.hostname === 'localhost' || location.hostname === '127.0.0.1' || location.hostname === '') {
		urlCheckEmail = 'http://localhost:4000/api/users/email-exists';
		urlRegister = 'http://localhost:4000/api/users/register';
	}
	else {
		urlCheckEmail = 'https://quiet-springs-95074.herokuapp.com/api/users/email-exists';
		urlRegister = 'https://quiet-springs-95074.herokuapp.com/api/users/register';
	}

	const config = {
		method: 'POST',
		headers: { 'Content-Type': 'application/json' },
		body: JSON.stringify({
			firstName: firstName,
			lastName: lastName,
			email: userEmail,
			mobileNo: mobileNumber,
			password: password1
		})
	};

	if (password1 !== password2) {
		alertify.set('notifier', 'position', 'top-center');
		alertify.error('Please make sure your password match');
	}
	else if (mobileNumber.length !== 12) {
		alertify.set('notifier', 'position', 'top-center');
		alertify.error('Invalid mobile number');
	}
	else {
		fetch(urlCheckEmail, config).then((res) => res.json()).then((data) => {
			if (!data) {
				fetch(urlRegister, config)
					.then((res) => {
						return res.json();
					})
					.then((data) => {
						if (data) {
							alertify.alert(
								'Success',
								'Congratulations, your account has been successfully created. You can now login',
								function (){
									window.location.replace('./login.html');
								}
							);
						}
						else {
							alertify.set('notifier', 'position', 'top-center');
							alertify.error('Something went wrong please try again or refresh the page');
						}
					});
			}
			else {
				alertify.set('notifier', 'position', 'top-center');
				alertify.error('The email address is already taken. Please choose another one');
			}
		});
	}
});

// const register = async () => {
// 	const resCheckEmail = await fetch(urlCheckEmail, config);
// 	const resEmailJSON = await resCheckEmail.json();
// 	if (!resEmailJSON) {
// 		const resRegister = await fetch(urlRegister, config);
// 		const resRegisterJSON = await resRegister.json();
// 		if (resRegisterJSON) {
// 			alert('User Added Successfully');
// 		} else {
// 			alert('There is something wrong');
// 		}
// 	} else {
// 		alert('Email already exist');
// 	}
// };

// register();

// Swal.fire({
// 	icon: 'success',
// 	title: 'Success',
// 	text: 'New user successfully added, You can now Login'
// }).then((result) => {
// 	if (result.isConfirmed) {
// 		window.location.replace('./login.html');
// 	}
// });
