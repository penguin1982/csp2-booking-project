let params = new URLSearchParams(window.location.search);
let id = params.get('courseId');
let userId = localStorage.getItem('id');

console.log(userId);

let token = localStorage.getItem('token');

if (!token || token === null) {
	window.location.href = './login.html';
}

let name = document.querySelector('#courseName');
let desc = document.querySelector('#courseDesc');
let price = document.querySelector('#coursePrice');
let enroll = document.querySelector('#enrollmentContainer');
let viewCourseUrl = '';
let enrollCourseUrl = '';

if (location.hostname === 'localhost' || location.hostname === '127.0.0.1' || location.hostname === '') {
	viewCourseUrl = `http://localhost:4000/api/courses/${id}`;
	enrollCourseUrl = `http://localhost:4000/api/users/enroll/`;
}
else {
	viewCourseUrl = `https://quiet-springs-95074.herokuapp.com/api/courses/${id}`;
	enrollCourseUrl = `https://quiet-springs-95074.herokuapp.com/api/users/enroll`;
}

fetch(viewCourseUrl).then((res) => res.json()).then((data) => {
	// console.log(data.enrollees.find((enrollee) => enrollee.userId == userId));
	const isEnroll = data.enrollees.find((enrollee) => enrollee.userId == userId);
	name.innerHTML = data.name;
	desc.innerHTML = data.description;
	price.innerHTML = data.price;
	enroll.innerHTML = `<button type="submit" class="btn ${isEnroll
		? 'btn-secondary'
		: 'btn-success'} text-white btn-block" ${isEnroll ? 'disabled' : ''} > ${isEnroll
		? 'Enrolled'
		: 'Enroll'}</button>`;

	// `<a id="enrollButton class="btn btn-success text-white btn-block">Enroll</button>`;

	const config = {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			Authorization: `Bearer ${token}`
		},
		body: JSON.stringify({
			courseId: data._id
		})
	};

	enroll.addEventListener('submit', (e) => {
		e.preventDefault();
		alertify.confirm(
			data.name,
			'Please confirm to enroll in this course',
			function (){
				fetch(enrollCourseUrl, config)
					.then((res) => {
						return res.json();
					})
					.then((data) => {
						if (data) {
							document.location.reload();
							document.onload = function (){
								alertify.set('notifier', 'position', 'top-center');
								alertify.success('Your enrollment request has been processed successfully', 6);
							};
						}
						else {
							alertify.set('notifier', 'position', 'top-center');
							alertify.error('You are unable to add this course at this time');
						}
					});
			},
			function (){
				alertify.set('notifier', 'position', 'top-center');
				alertify.notify('Cancelled');
			}
		);
	});
});
