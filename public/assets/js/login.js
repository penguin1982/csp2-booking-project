const loginForm = document.querySelector('#loginUser');

loginForm.addEventListener('submit', (e) => {
	e.preventDefault();
	const email = document.querySelector('#userEmail').value;
	const password = document.querySelector('#password').value;
	let urlLogin = '';
	let urlUserDetails = '';

	if (location.hostname === 'localhost' || location.hostname === '127.0.0.1' || location.hostname === '') {
		urlLogin = 'http://localhost:4000/api/users/login';
		urlUserDetails = 'http://localhost:4000/api/users/details';
	}
	else {
		urlLogin = 'https://quiet-springs-95074.herokuapp.com/api/users/login';
		urlUserDetails = 'https://quiet-springs-95074.herokuapp.com/api/users/details';
	}

	const config = {
		method: 'POST',
		headers: { 'Content-Type': 'application/json' },
		body: JSON.stringify({
			email: email,
			password: password
		})
	};

	if (email == '' || password == '') {
		alertify.set('notifier', 'position', 'top-center');
		alertify.error('Please input your username and password');
	}
	else {
		fetch(urlLogin, config)
			.then((res) => {
				return res.json();
			})
			.then((data) => {
				if (data.access) {
					localStorage.setItem('token', data.access);
					fetch(urlUserDetails, {
						headers: {
							Authorization: `Bearer ${data.access}`
						}
					})
						.then((res) => res.json())
						.then((data) => {
							console.log(data);
							localStorage.setItem('id', data._id);
							localStorage.setItem('isAdmin', data.isAdmin);
							window.location.replace('./courses.html');
						});
				}
				else {
					alertify.set('notifier', 'position', 'top-center');
					alertify.error('Incorrect username or password');
				}
			});
	}
});
