let token = localStorage.getItem('token');
const profile = document.getElementById('profileContainer');
let urlDetails = '';

if (location.hostname === 'localhost' || location.hostname === '127.0.0.1' || location.hostname === '') {
	urlDetails = 'http://localhost:4000/api/users/details';
}
else {
	urlDetails = 'https://quiet-springs-95074.herokuapp.com/api/users/details';
}

const config = {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json',
		Authorization: `Bearer ${token}`
	}
};

if (!token || token === null) {
	window.location.href = './login.html';
	// alert('You must login first');
}
else {
	fetch(urlDetails, config).then((res) => res.json()).then((data) => {
		console.log(data);
		// const user = data.pop();
		// console.log(user);

		const enrollmentData = data.enrollments
			.map((course) => {
				// let enrolledOn = course.enrolledOn;
				// enrolledOn.toDateString();
				return `
				<tr>
					<td>${course.courseId}</td>
					<td>${course.courseName}</td>
					<td>${course.enrolledOn}</td>
					<td>${course.status}</td>
				</tr>
			`;
			})
			.join('');

		profile.innerHTML = `
		<div class="col-md-12">
			<section class="jumbotron my-auto card">
				<h3 class="text-center">First Name: ${data.firstName} </h3>
				<h3 class="text-center">Last Name: ${data.lastName}</h3>
				<h3 class="text-center">Email: ${data.email}</h3>

				<table class="table mt-4">
					<thead>
						<tr>
							<th>Course ID:</th>
							<th>Course Name:</th>
							<th>Enrolled On:</th>
							<th>Status:</th>
							<tbody>${enrollmentData}</tbody>
						</tr>
					</thead>
				</table>

			</section>
		</div>
		`;
	});
}
